cmake_minimum_required(VERSION 3.10)

# set the project name
project(TestServer)

# add the executable
add_executable(TestServer main.cpp ClassFactory.cpp
			TestServer.cpp
			TestServerClass.cpp
			TestServerStateMachine.cpp)

target_include_directories(TestServer PRIVATE "${PROJECT_DIR}" ../include )
target_link_directories(TestServer PRIVATE ../lib )
target_link_libraries(TestServer PRIVATE COS421_rtd.lib 
										 libzmq-v141-mt-gd-4_0_5.lib
										 omniDynamic421_rtd.lib
										 omniORB421_rtd.lib
										 omnithread40_rtd.lib
										 tangod.lib)
